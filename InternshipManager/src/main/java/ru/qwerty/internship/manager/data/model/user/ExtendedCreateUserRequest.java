package ru.qwerty.internship.manager.data.model.user;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;
import ru.qwerty.internship.manager.data.entity.Role;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Jacksonized
public class ExtendedCreateUserRequest extends CreateUserRequest {

    @NotNull(message = Messages.INVALID_ROLE)
    private final Role role;
}
