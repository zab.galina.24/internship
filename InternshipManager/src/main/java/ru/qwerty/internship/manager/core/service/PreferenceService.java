package ru.qwerty.internship.manager.core.service;

import ru.qwerty.internship.manager.data.model.preference.CreatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;
import ru.qwerty.internship.manager.data.model.preference.UpdatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserProfileWithPreferences;

import java.util.List;
import java.util.Map;

public interface PreferenceService {

    List<PreferenceResponse> get();

    List<UserProfileWithPreferences> getAll();

    PreferenceResponse create(CreatePreferenceRequest request);

    PreferenceResponse update(UpdatePreferenceRequest request);
}
