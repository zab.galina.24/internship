package ru.qwerty.internship.manager.data.entity;

public enum Role {

    UNVERIFIED_STUDENT,

    STUDENT,

    DEAN,

    COMPANY,

    ADMIN
}
