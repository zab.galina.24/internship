package ru.qwerty.internship.manager.data.mapper;

import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.entity.VacancyEntity;
import ru.qwerty.internship.manager.data.model.vacancy.CreateVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

public interface VacancyMapper {

    VacancyEntity map(CreateVacancyRequest createVacancyRequest, CompanyEntity companyEntity);

    VacancyResponse map(VacancyEntity vacancyEntity);
}
