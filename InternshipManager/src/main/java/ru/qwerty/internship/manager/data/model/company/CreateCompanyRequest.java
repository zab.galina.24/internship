package ru.qwerty.internship.manager.data.model.company;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Value
@Jacksonized
@Builder(builderClassName = "Builder")
public class CreateCompanyRequest {

    @NotEmpty(message = Messages.INVALID_COMPANY_NAME)
    String name;

    String description;

    String location;

    String email;

    String phoneNumber;

}
