package ru.qwerty.internship.manager.data.mapper.implement;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.entity.VacancyEntity;
import ru.qwerty.internship.manager.data.mapper.VacancyMapper;
import ru.qwerty.internship.manager.data.model.vacancy.CreateVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

@Component
@RequiredArgsConstructor
public class CommonVacancyMapper implements VacancyMapper {

    @Override
    public VacancyEntity map(CreateVacancyRequest createVacancyRequest, CompanyEntity companyEntity) {
        return VacancyEntity.builder()
                .name(createVacancyRequest.getName())
                .techStack(createVacancyRequest.getTechStack())
                .minimumQuality(createVacancyRequest.getMinimumQuantity())
                .maximumQuality(createVacancyRequest.getMaximumQuantity())
                .company(companyEntity)
                .build();
    }

    @Override
    public VacancyResponse map(VacancyEntity vacancyEntity) {
        return VacancyResponse.builder()
                .id(vacancyEntity.getId())
                .name(vacancyEntity.getName())
                .techStack(vacancyEntity.getTechStack())
                .minimumQuality(vacancyEntity.getMinimumQuality())
                .maximumQuality(vacancyEntity.getMaximumQuality())
                .companyId(vacancyEntity.getCompany() != null ? vacancyEntity.getCompany().getId() : null)
                .build();
    }
}
