package ru.qwerty.internship.manager.data;

public final class Messages {

    public static final String INVALID_USERNAME = "invalid username";

    public static final String INVALID_PASSWORD = "invalid password";

    public static final String INVALID_FIRSTNAME = "invalid firstname";

    public static final String INVALID_LASTNAME = "invalid lastname";

    public static final String INVALID_GROUP_NUMBER = "invalid group number";

    public static final String INVALID_TOKEN = "invalid token";

    public static final String INVALID_ROLE = "invalid role";

    public static final String INVALID_COMPANY_ID = "invalid company id";

    public static final String INVALID_COMPANY_NAME = "invalid company name";

    public static final String INVALID_VACANCY_NAME = "invalid vacancy name";

    public static final String INVALID_TECH_STACK = "invalid tech stack";

    public static final String INVALID_MIN_VACANCY_QUANTITY = "invalid min vacancy quantity";

    public static final String INVALID_MAX_VACANCY_QUANTITY = "invalid max vacancy quantity";

    public static final String INVALID_VACANCY_ID = "invalid vacancy id";

    public static final String INVALID_ORDER_NUMBER = "invalid order number";

    public static final String INVALID_PREFERENCE_ID = "invalid preference id";

    private Messages() {
    }
}
