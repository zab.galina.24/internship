package ru.qwerty.internship.manager.data.model.auth;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Value
@Builder
@Jacksonized
public class RegisterRequest {

    @NotEmpty(message = Messages.INVALID_USERNAME)
    String username;

    @NotEmpty(message = Messages.INVALID_PASSWORD)
    String password;

    @NotEmpty(message = Messages.INVALID_FIRSTNAME)
    String firstname;

    String patronymic;

    @NotEmpty(message = Messages.INVALID_LASTNAME)
    String lastname;

    @NotEmpty(message = Messages.INVALID_GROUP_NUMBER)
    String groupNumber;
}
