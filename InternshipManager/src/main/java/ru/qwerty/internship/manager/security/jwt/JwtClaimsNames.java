package ru.qwerty.internship.manager.security.jwt;

final class JwtClaimsNames {

    public static final String ROLE = "role";

    private JwtClaimsNames() {
    }
}
