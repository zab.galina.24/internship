package ru.qwerty.internship.manager.core.validation;

import ru.qwerty.internship.manager.data.Messages;
import ru.qwerty.internship.manager.data.Parameters;
import ru.qwerty.internship.manager.exception.InvalidArgumentException;

public final class Validator {

    private Validator() {
    }

    public static void checkGroupNumber(String groupNumber) {
        if (groupNumber.length() != Parameters.GROUP_LENGTH) {
            throw new InvalidArgumentException(Messages.INVALID_GROUP_NUMBER);
        }

        if (!groupNumber.startsWith(Parameters.GROUP_PREFIX)) {
            throw new InvalidArgumentException(Messages.INVALID_GROUP_NUMBER);
        }
    }
}
