package ru.qwerty.internship.manager.data.model.preference;

public enum Status {
    IN_PROGRESS,
    REJECTED,
    ACCEPTED,
    NOT_STARTED
}
