package ru.qwerty.internship.manager.data.model.vacancy;

import lombok.Builder;
import lombok.Value;
import org.openapitools.jackson.nullable.JsonNullable;

@Value
@Builder(builderClassName = "Builder")
public class EditVacancyRequest {

    JsonNullable<String> name = JsonNullable.undefined();

    JsonNullable<String> techStack = JsonNullable.undefined();

//    @Positive(message = Messages.INVALID_MIN_VACANCY_QUANTITY)
    JsonNullable<Integer> minimumQuantity = JsonNullable.undefined();

//    @Positive(message = Messages.INVALID_MAX_VACANCY_QUANTITY)
    JsonNullable<Integer> maximumQuantity = JsonNullable.undefined();
}
