package ru.qwerty.internship.manager.core.service;

import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.company.CreateCompanyRequest;
import ru.qwerty.internship.manager.data.model.company.EditCompanyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

public interface CompanyService {

    List<CompanyResponse> get();

    CompanyResponse getById(long id);

    CompanyEntity getEntityById(long id);

    CompanyResponse create(CreateCompanyRequest company);

    CompanyResponse patch(Long id, EditCompanyRequest company);

    void delete(long id);

    List<VacancyResponse> getVacancies(long companyId);
}
