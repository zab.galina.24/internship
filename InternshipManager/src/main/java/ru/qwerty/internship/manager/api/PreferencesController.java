package ru.qwerty.internship.manager.api;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.qwerty.internship.manager.core.security.SecurityExpressions;
import ru.qwerty.internship.manager.core.service.PreferenceService;
import ru.qwerty.internship.manager.data.model.preference.CreatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;
import ru.qwerty.internship.manager.data.model.preference.UpdatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserProfileWithPreferences;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/preferences")
@RequiredArgsConstructor
public class PreferencesController {

    private final PreferenceService service;

    @GetMapping
    @PreAuthorize(SecurityExpressions.ROLE_STUDENT)
    public List<PreferenceResponse> get() {
        return service.get();
    }

    @PostMapping
    @PreAuthorize(SecurityExpressions.ROLE_STUDENT)
    public PreferenceResponse create(@Valid @RequestBody CreatePreferenceRequest request) {
        return service.create(request);
    }

    @PatchMapping
    @PreAuthorize(SecurityExpressions.ROLE_STUDENT)
    public PreferenceResponse update(@Valid @RequestBody UpdatePreferenceRequest request) {
        return service.update(request);
    }

    @GetMapping("/all")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public List<UserProfileWithPreferences> getAll() {
        return service.getAll();
    }
}
