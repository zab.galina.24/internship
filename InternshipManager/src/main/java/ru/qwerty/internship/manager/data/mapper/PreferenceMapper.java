package ru.qwerty.internship.manager.data.mapper;

import ru.qwerty.internship.manager.data.entity.PreferenceEntity;
import ru.qwerty.internship.manager.data.model.preference.CreatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;

public interface PreferenceMapper {

    PreferenceEntity map(CreatePreferenceRequest request);

    PreferenceResponse map(PreferenceEntity entity);
}
