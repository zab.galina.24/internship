package ru.qwerty.internship.manager.data.model.user;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.entity.Role;

@Data
@Builder
@Jacksonized
public class UserProfile {

    private Long id;

    private byte[] profilePhoto;

    private String username;

    private String firstname;

    private String patronymic;

    private String lastname;

    private Role role;

    private String birthDate;

    private double performance;

    private int studyYear;

    private String groupNumber;

    private String internshipCompany;

    private String responsibleForCompany;

    private String placeOfWork;

    private String jobTitle;

}
