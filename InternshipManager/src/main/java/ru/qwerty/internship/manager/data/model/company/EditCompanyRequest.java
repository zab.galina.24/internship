package ru.qwerty.internship.manager.data.model.company;

import lombok.Builder;
import lombok.Value;
import org.openapitools.jackson.nullable.JsonNullable;

@Value
@Builder(builderClassName = "Builder")
public class EditCompanyRequest {

    JsonNullable<String> name = JsonNullable.undefined();

    JsonNullable<String> description = JsonNullable.undefined();

    JsonNullable<String> location = JsonNullable.undefined();

    JsonNullable<String> email = JsonNullable.undefined();

    JsonNullable<String> phoneNumber = JsonNullable.undefined();

}
