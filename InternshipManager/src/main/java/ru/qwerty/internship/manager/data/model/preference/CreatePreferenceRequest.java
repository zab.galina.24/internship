package ru.qwerty.internship.manager.data.model.preference;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;
import ru.qwerty.internship.manager.data.Parameters;

@Value
@Builder
@Jacksonized
public class CreatePreferenceRequest {

    @NotNull(message = Messages.INVALID_VACANCY_ID)
    Long vacancyId;

    @Min(value = Parameters.MIN_PREFERENCE_ORDER_NUMBER, message = Messages.INVALID_ORDER_NUMBER)
    @Max(value = Parameters.MAX_PREFERENCE_ORDER_NUMBER, message = Messages.INVALID_ORDER_NUMBER)
    Integer orderNumber;

    Status status;
}
