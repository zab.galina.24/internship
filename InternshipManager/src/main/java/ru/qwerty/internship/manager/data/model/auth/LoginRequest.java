package ru.qwerty.internship.manager.data.model.auth;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Value
@Builder
@Jacksonized
public class LoginRequest {

    @NotEmpty(message = Messages.INVALID_USERNAME)
    String username;

    @NotEmpty(message = Messages.INVALID_PASSWORD)
    String password;
}
