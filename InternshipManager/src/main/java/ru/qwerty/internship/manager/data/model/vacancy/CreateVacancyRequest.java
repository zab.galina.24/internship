package ru.qwerty.internship.manager.data.model.vacancy;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Value
@Jacksonized
@Builder(builderClassName = "Builder")
public class CreateVacancyRequest {

    @NotEmpty(message = Messages.INVALID_VACANCY_NAME)
    String name;

    @NotNull(message = Messages.INVALID_TECH_STACK)
    String techStack;

    @Positive(message = Messages.INVALID_MIN_VACANCY_QUANTITY)
    int minimumQuantity;

    @Positive(message = Messages.INVALID_MAX_VACANCY_QUANTITY)
    int maximumQuantity;
}
