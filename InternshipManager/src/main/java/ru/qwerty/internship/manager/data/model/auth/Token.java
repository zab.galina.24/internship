package ru.qwerty.internship.manager.data.model.auth;

import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Value
@Jacksonized
@Builder
public class Token {

    @NotEmpty(message = Messages.INVALID_TOKEN)
    String value;
}
