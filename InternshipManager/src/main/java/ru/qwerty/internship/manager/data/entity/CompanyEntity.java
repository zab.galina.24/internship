package ru.qwerty.internship.manager.data.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Entity
@Builder(toBuilder = true)
@Table(name = "company")
@NoArgsConstructor
@AllArgsConstructor
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String description;

    private String location;

    private String email;

    private String phoneNumber;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    private List<CompanyDelegateEntity> delegates;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    private List<VacancyEntity> vacancies;
}
