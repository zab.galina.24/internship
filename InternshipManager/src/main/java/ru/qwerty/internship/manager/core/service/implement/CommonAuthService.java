package ru.qwerty.internship.manager.core.service.implement;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.qwerty.internship.manager.core.service.AuthService;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.core.validation.Validator;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.mapper.UserMapper;
import ru.qwerty.internship.manager.data.model.auth.LoginRequest;
import ru.qwerty.internship.manager.data.model.auth.RegisterRequest;
import ru.qwerty.internship.manager.data.model.auth.Token;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;
import ru.qwerty.internship.manager.data.model.auth.TokenType;
import ru.qwerty.internship.manager.data.model.user.UserProfileAndToken;
import ru.qwerty.internship.manager.exception.InternalException;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;
import ru.qwerty.internship.manager.exception.ServiceException;
import ru.qwerty.internship.manager.exception.UnauthorizedException;
import ru.qwerty.internship.manager.security.JwtService;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonAuthService implements AuthService {

    private final JwtService jwtService;

    private final UserService userService;

    private final UserMapper userMapper;

    @Override
    @Transactional
    public TokenPair register(RegisterRequest request) {
        log.info("Register user: username = {}", request.getUsername());
        Validator.checkGroupNumber(request.getGroupNumber());

        UserEntity user = userService.create(userMapper.map(request));
        return jwtService.generateTokenPair(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserProfileAndToken login(LoginRequest request) {
        UserProfileAndToken response = new UserProfileAndToken();
        log.info("Login: username = {}", request.getUsername());

        try {
            UserEntity user = userService.getByUsername(request.getUsername());
            if (BCrypt.checkpw(request.getPassword(), user.getHashedPassword())) {
                response.setTokenPair(jwtService.generateTokenPair(user));
            } else {
                throw new UnauthorizedException("Invalid password");
            }
            response.setUserProfile(userMapper.mapToProfile(user));
        } catch (Exception e) {
            throw convertException(e);
        }
        return response;
    }

    @Override
    @Transactional(readOnly = true)
    public TokenPair refreshToken(Token token) {
        log.info("Refresh token: value = {}", token.getValue());

        try {
            jwtService.validateRefreshToken(token.getValue());
            Claims claims = jwtService.extractClaims(token.getValue(), TokenType.REFRESH);
            UserEntity user = userService.getByUsername(claims.getSubject());
            return jwtService.generateTokenPair(user);
        } catch (Exception e) {
            throw convertException(e);
        }
    }

    private static ServiceException convertException(Exception exception) {
        if (exception instanceof UnauthorizedException unauthorizedException) {
            return unauthorizedException;
        }
        if (exception instanceof ObjectNotFoundException) {
            return new UnauthorizedException(exception.getMessage());
        }
        if (exception instanceof JwtException) {
            return new UnauthorizedException("Invalid jwt");
        }

        return new InternalException();
    }
}
