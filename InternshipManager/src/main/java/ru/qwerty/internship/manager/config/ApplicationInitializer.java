package ru.qwerty.internship.manager.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.config.property.AdminProperties;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.data.entity.Role;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

@Slf4j
@Component
@RequiredArgsConstructor
public class ApplicationInitializer {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    private final AdminProperties adminProperties;

    @EventListener(ContextRefreshedEvent.class)
    public void registerAdmin() {
        try {
            deleteAdminQuietly();
            userService.create(createAdmin());
            log.info("Register admin with username = {}", adminProperties.getUsername());
        } catch (Exception exception) {
            log.error("Failed to register admin", exception);
        }
    }

    private void deleteAdminQuietly() {
        try {
            userService.deleteByUsername(adminProperties.getUsername());
        } catch (ObjectNotFoundException e) {
            // Ignored.
        }
    }

    private UserEntity createAdmin() {
        return UserEntity.builder()
                .username(adminProperties.getUsername())
                .hashedPassword(passwordEncoder.encode(adminProperties.getPassword()))
                .role(Role.ADMIN)
                .build();
    }
}
