package ru.qwerty.internship.manager.data.model.user;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.Messages;

@Data
@SuperBuilder
@Jacksonized
public class CreateUserRequest {

    @NotEmpty(message = Messages.INVALID_USERNAME)
    private final String username;

    @NotEmpty(message = Messages.INVALID_PASSWORD)
    private final String password;

    private final String firstname;

    private final String patronymic;

    private final String lastname;

    private final String groupNumber;

    private final Long companyId;


    private byte[] profilePhoto;

    private String birthDate;

    private double performance;

    private int studyYear;

    private String internshipCompany;

    private String responsibleForCompany;

    private String jobTitle;
}
