package ru.qwerty.internship.manager.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public abstract class ServiceException extends RuntimeException {

    @Getter
    private final HttpStatus httpStatus;

    protected ServiceException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
