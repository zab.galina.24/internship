package ru.qwerty.internship.manager.data.model.auth;

public enum TokenType {
    AUTHORIZATION,
    REFRESH
}
