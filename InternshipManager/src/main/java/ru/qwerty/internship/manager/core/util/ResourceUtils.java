package ru.qwerty.internship.manager.core.util;

import lombok.experimental.UtilityClass;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import ru.qwerty.internship.manager.data.model.resource.ResourceResponse;

@UtilityClass
public class ResourceUtils {

    public ResponseEntity<Resource> createResponseEntity(ResourceResponse resource) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentDisposition(ContentDisposition.attachment().filename(resource.getContentName()).build());

        return ResponseEntity.ok()
                .headers(httpHeaders)
                .contentLength(resource.getContentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource.getResource());
    }
}
