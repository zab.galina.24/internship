package ru.qwerty.internship.manager.config.property;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminProperties {

    private String username;

    private String password;
}
