package ru.qwerty.internship.manager.api.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import ru.qwerty.internship.manager.exception.ServiceException;

@Slf4j
@Component
@RestControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handle(Exception exception) {
        if (exception instanceof ServiceException serviceException) {
            log.warn("Service exception was handled: message = {}", serviceException.getMessage());
            return new ResponseEntity<>(new Error(serviceException.getMessage()), serviceException.getHttpStatus());
        }

        if (exception instanceof MethodArgumentNotValidException methodArgumentNotValidException) {
            String errorMessage = methodArgumentNotValidException.getAllErrors().get(0).getDefaultMessage();
            log.warn("Invalid method argument: message = {}", errorMessage);
            return new ResponseEntity<>(new Error(errorMessage), HttpStatus.BAD_REQUEST);
        }

        if (exception instanceof AccessDeniedException) {
            log.warn("Access denied: message = {}", exception.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        if (exception instanceof MissingServletRequestParameterException) {
            log.info("Missing request param: message = {}", exception.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (exception instanceof MultipartException) {
            log.info("Error with multipart file", exception);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (exception instanceof HttpMessageNotReadableException) {
            log.warn("Invalid request body: message = {}", exception.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (exception instanceof HttpMediaTypeNotSupportedException) {
            log.warn("Http media type not supported: message = {}", exception.getMessage());
            return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }

        if (exception instanceof HttpRequestMethodNotSupportedException) {
            log.warn("Http method not supported: message = {}", exception.getMessage());
            return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
        }

        if (exception instanceof NoHandlerFoundException) {
            log.warn("No handler found: message = {}", exception.getMessage());
            return new ResponseEntity<>(new Error("No handler found"), HttpStatus.NOT_FOUND);
        }

        if (exception instanceof ClassCastException) {
            log.error("Failed to cast", exception);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        log.error("Failed to handle exception", exception);
        return new ResponseEntity<>(new Error("Internal server error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
