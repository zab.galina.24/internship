package ru.qwerty.internship.manager.data.mapper.implement;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.entity.VacancyEntity;
import ru.qwerty.internship.manager.data.mapper.CompanyMapper;
import ru.qwerty.internship.manager.data.mapper.VacancyMapper;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.company.CreateCompanyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CommonCompanyMapper implements CompanyMapper {

    private final VacancyMapper vacancyMapper;

    private final VacancyService vacancyService;

    @Override
    public CompanyEntity map(CreateCompanyRequest createCompanyRequest) {
        return CompanyEntity.builder()
                .name(createCompanyRequest.getName())
                .description(createCompanyRequest.getDescription())
                .email(createCompanyRequest.getEmail())
                .phoneNumber(createCompanyRequest.getPhoneNumber())
                .location(createCompanyRequest.getLocation())
                .build();
    }

    @Override
    public CompanyResponse map(CompanyEntity companyEntity) {
        CompanyResponse.Builder builder = CompanyResponse.builder()
                .id(companyEntity.getId())
                .name(companyEntity.getName())
                .description(companyEntity.getDescription())
                .email(companyEntity.getEmail())
                .phoneNumber(companyEntity.getPhoneNumber())
                .location(companyEntity.getLocation());
        if (companyEntity.getVacancies() != null) {
            List<VacancyResponse> vacancyResponseList = new java.util.ArrayList<>(List.of());
            for (VacancyResponse vacancyResponse: vacancyService.get()) {
                if (vacancyResponse.getCompanyId() == companyEntity.getId()) {
                    vacancyResponseList.add(vacancyResponse);
                }
            }
            return builder.vacancies(vacancyResponseList).build();
        } else {
            return builder.build();
        }
    }
}
