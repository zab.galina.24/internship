package ru.qwerty.internship.manager.data.model.resource;

import lombok.Builder;
import lombok.Value;
import org.springframework.core.io.Resource;

@Value
@Builder
public class ResourceResponse {

    Resource resource;

    long contentLength;

    String contentName;
}
