package ru.qwerty.internship.manager.data.mapper;

import org.springframework.web.multipart.MultipartFile;
import ru.qwerty.internship.manager.data.entity.DiaryEntity;
import ru.qwerty.internship.manager.data.model.diary.DiaryResponse;

public interface DiaryMapper {

    DiaryEntity map(MultipartFile file);

    DiaryResponse map(DiaryEntity entity);
}
