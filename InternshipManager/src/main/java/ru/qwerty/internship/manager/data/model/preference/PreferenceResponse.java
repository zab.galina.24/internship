package ru.qwerty.internship.manager.data.model.preference;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.core.repository.CompanyRepository;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

@Value
@Builder
@Jacksonized
public class PreferenceResponse {

    long id;

    long vacancyId;

    long studentId;

    int orderNumber;

    Status status;

    CompanyResponse company;

    VacancyResponse vacancy;
}
