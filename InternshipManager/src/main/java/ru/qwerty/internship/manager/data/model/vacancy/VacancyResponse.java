package ru.qwerty.internship.manager.data.model.vacancy;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder(builderClassName = "Builder")
public class VacancyResponse {

    Long id;

    String name;

    String techStack;

    int minimumQuality;

    int maximumQuality;

    Long companyId;
}
