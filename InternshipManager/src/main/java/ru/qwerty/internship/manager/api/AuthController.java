package ru.qwerty.internship.manager.api;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.qwerty.internship.manager.core.service.AuthService;
import ru.qwerty.internship.manager.data.model.auth.LoginRequest;
import ru.qwerty.internship.manager.data.model.auth.RegisterRequest;
import ru.qwerty.internship.manager.data.model.auth.Token;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;
import ru.qwerty.internship.manager.data.model.user.UserProfileAndToken;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

//    @PostMapping("/register")
//    public TokenPair register(@Valid @RequestBody RegisterRequest request) {
//        return authService.register(request);
//    }

    @PostMapping("/login")
    public UserProfileAndToken login(@Valid @RequestBody LoginRequest request) {
        return authService.login(request);
    }

    @PostMapping("/refresh")
    public TokenPair refreshToken(@Valid @RequestBody Token token) {
        return authService.refreshToken(token);
    }
}
