package ru.qwerty.internship.manager.data.model.company;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

@Value
@Jacksonized
@Builder(builderClassName = "Builder")
public class CompanyResponse {

    long id;

    String name;

    String description;

    String location;

    String email;

    String phoneNumber;

    List<VacancyResponse> vacancies;

}
