package ru.qwerty.internship.manager.core.service.implement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.qwerty.internship.manager.core.repository.CompanyRepository;
import ru.qwerty.internship.manager.core.service.CompanyService;
import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.mapper.CompanyMapper;
import ru.qwerty.internship.manager.data.mapper.VacancyMapper;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.company.CreateCompanyRequest;
import ru.qwerty.internship.manager.data.model.company.EditCompanyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonCompanyService implements CompanyService {

    private static final String COMPANY_NOT_FOUND_MESSAGE_TEMPLATE = "Company with id = %d not found";

    private final CompanyRepository repository;

    private final CompanyMapper mapper;

    private final VacancyMapper vacancyMapper;

    @Override
    @Transactional(readOnly = true)
    public List<CompanyResponse> get() {
        log.info("Get all companies");
        List<CompanyResponse> companies = new ArrayList<>();
        repository.findAll().forEach(companyEntity -> companies.add(mapper.map(companyEntity)));
        return companies;
    }

    @Override
    @Transactional(readOnly = true)
    public CompanyResponse getById(long id) {
        log.info("Get company by id = {}", id);
        CompanyEntity companyEntity = getEntityById(id);
        return mapper.map(companyEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public CompanyEntity getEntityById(long id) {
        log.info("Get company entity by id = {}", id);
        return repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(String.format(COMPANY_NOT_FOUND_MESSAGE_TEMPLATE, id))
        );
    }

    @Override
    @Transactional
    public CompanyResponse create(CreateCompanyRequest company) {
        log.info("Create company = {}", company);
        CompanyEntity companyEntity = mapper.map(company);
        return mapper.map(repository.save(companyEntity));
    }

    @Override
    @Transactional
    public CompanyResponse patch(Long id, EditCompanyRequest company) {
        CompanyEntity companyEntity = getEntityById(id);
        if (company.getName().isPresent()) {
            companyEntity.setName(company.getName().get());
        }
        if (company.getDescription().isPresent()) {
            companyEntity.setDescription(company.getDescription().get());
        }
        if (company.getLocation().isPresent()) {
            companyEntity.setLocation(company.getLocation().get());
        }
        if (company.getEmail().isPresent()) {
            companyEntity.setEmail(company.getEmail().get());
        }
        if (company.getPhoneNumber().isPresent()) {
            companyEntity.setPhoneNumber(company.getPhoneNumber().get());
        }
        repository.save(companyEntity);

        return mapper.map(companyEntity);
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.info("Delete company by id = {}", id);
        CompanyEntity companyEntity = getEntityById(id);
        repository.delete(companyEntity);
    }

    @Override
    @Transactional
    public List<VacancyResponse> getVacancies(long companyId) {
        CompanyEntity companyEntity = getEntityById(companyId);
        return companyEntity.getVacancies().stream().map(vacancyMapper::map).toList();
    }
}
