package ru.qwerty.internship.manager.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String hashedPassword;

    private String firstname;

    private String patronymic;

    private String lastname;

    private byte[] profilePhoto;

    private String birthDate;

    private double performance;

    private int studyYear;

    private String groupNumber;

    private String internshipCompany;

    private String responsibleForCompany;

    private String placeOfWork;

    private String jobTitle;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @SuppressWarnings("unchecked")
    public <T extends UserEntity> T cast() {
        return (T) this;
    }
}
