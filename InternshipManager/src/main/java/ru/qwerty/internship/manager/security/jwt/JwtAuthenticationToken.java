package ru.qwerty.internship.manager.security.jwt;

import io.jsonwebtoken.Claims;
import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private final String value;

    private final Claims claims;

    public JwtAuthenticationToken(String value) {
        super(null);
        setAuthenticated(false);

        this.value = value;
        this.claims = null;
    }

    public JwtAuthenticationToken(Claims claims, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        setAuthenticated(true);

        this.value = null;
        this.claims = claims;
    }

    @Override
    public String getCredentials() {
        return value;
    }

    @Override
    public Claims getPrincipal() {
        return claims;
    }
}
