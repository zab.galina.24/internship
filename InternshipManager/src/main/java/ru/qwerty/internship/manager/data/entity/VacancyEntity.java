package ru.qwerty.internship.manager.data.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@Table(name = "vacancy")
@NoArgsConstructor
@AllArgsConstructor
public class VacancyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String techStack;

    private int minimumQuality;

    private int maximumQuality;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private CompanyEntity company;

    @OneToMany(mappedBy = "vacancy", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PreferenceEntity> preferences;
}
