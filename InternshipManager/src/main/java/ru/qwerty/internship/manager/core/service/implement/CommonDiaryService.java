package ru.qwerty.internship.manager.core.service.implement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.qwerty.internship.manager.core.repository.DiaryRepository;
import ru.qwerty.internship.manager.core.service.DiaryService;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.data.entity.DiaryEntity;
import ru.qwerty.internship.manager.data.entity.StudentEntity;
import ru.qwerty.internship.manager.data.mapper.DiaryMapper;
import ru.qwerty.internship.manager.data.model.diary.DiaryResponse;
import ru.qwerty.internship.manager.data.model.resource.ResourceResponse;
import ru.qwerty.internship.manager.exception.InternalException;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Service
public class CommonDiaryService implements DiaryService {

    private static final String DIARY_NOT_FOUND = "Diary with id = %s not found";

    private static final String UPLOADS_PATH = Paths.get("src/main/resources/uploads").toAbsolutePath() + "/";

    private final UserService userService;

    private final DiaryMapper mapper;

    private final DiaryRepository repository;

    public CommonDiaryService(UserService userService, DiaryMapper mapper, DiaryRepository repository) {
        this.userService = userService;
        this.mapper = mapper;
        this.repository = repository;

        File uploadsDir = new File(UPLOADS_PATH);
        if (!uploadsDir.exists()) {
            uploadsDir.mkdir();
        }
    }

    @Override
    public ResponseEntity<byte[]> get(long studentId, long diaryId) {
        log.info("Download diary content: studentId = {}, diaryId = {}", studentId, diaryId);
        DiaryEntity entity = repository.findById(diaryId).orElseThrow(
                () -> new ObjectNotFoundException(String.format(DIARY_NOT_FOUND, diaryId))
        );

        if (entity.getStudent().getId() != studentId) {
            throw new ObjectNotFoundException(String.format(DIARY_NOT_FOUND, diaryId));
        }

        try {
            byte[] fileContent = Files.readAllBytes(Paths.get(UPLOADS_PATH + entity.getPath()));
            return ResponseEntity.ok()
                    .header("Content-Disposition", String.format("attachment; filename=%s", entity.getOriginalName()))
                    .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document"))
                    .body(fileContent);
        } catch (Exception e) {
            throw new InternalException();
        }
    }

    @Override
    @Transactional
    public DiaryResponse create(MultipartFile file) {
        if (file.isEmpty()) {
            return null;
        }
        log.info("Create diary");

        DiaryEntity diary = mapper.map(file);
        saveFile(file, diary.getPath());
        return mapper.map(repository.save(diary));
    }

    @Override
    @Transactional(readOnly = true)
    public List<DiaryResponse> get(Long studentId) {
        log.info("Get diaries by student id = {}", studentId);
        StudentEntity student = userService.getById(studentId).cast();
        return student.getDiaries().stream().map(mapper::map).toList();
    }

    @Override
    public List<DiaryResponse> get() {
        log.info("Get all diaries");
        List<DiaryEntity> diaryEntities = (List<DiaryEntity>) repository.findAll();
        return diaryEntities.stream().map(mapper::map).toList();
    }

    private static void saveFile(MultipartFile multipartFile, String filepath) {
        try {
            String fullFilepath = UPLOADS_PATH + filepath;
            log.info("Save file to path = {}", fullFilepath);

            multipartFile.transferTo(new File(fullFilepath));
        } catch (Exception e) {
            log.error("Failed to save diary", e);
            throw new InternalException();
        }
    }
}
