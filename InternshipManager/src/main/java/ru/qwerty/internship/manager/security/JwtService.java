package ru.qwerty.internship.manager.security;

import io.jsonwebtoken.Claims;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;
import ru.qwerty.internship.manager.data.model.auth.TokenType;

public interface JwtService {

    void validateAuthToken(String token);

    void validateRefreshToken(String token);

    TokenPair generateTokenPair(UserEntity user);

    Claims extractClaims(String token, TokenType tokenType);
}
