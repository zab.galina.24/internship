package ru.qwerty.internship.manager.api;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.qwerty.internship.manager.core.security.SecurityExpressions;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

@RestController
@RequestMapping("/vacancies")
@RequiredArgsConstructor
public class VacancyController {

    private final VacancyService service;

    @GetMapping
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public List<VacancyResponse> get() {
        return service.get();
    }

    @GetMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public VacancyResponse getById(@PathVariable long id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public void delete(@PathVariable long id) {
        service.delete(id);
    }
}
