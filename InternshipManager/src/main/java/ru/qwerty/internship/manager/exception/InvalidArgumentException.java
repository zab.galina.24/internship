package ru.qwerty.internship.manager.exception;

import org.springframework.http.HttpStatus;

public class InvalidArgumentException extends ServiceException {

    public InvalidArgumentException() {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
    }

    public InvalidArgumentException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
