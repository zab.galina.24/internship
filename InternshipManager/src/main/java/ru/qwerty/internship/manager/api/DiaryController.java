package ru.qwerty.internship.manager.api;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.qwerty.internship.manager.core.context.ExecutionContextHolder;
import ru.qwerty.internship.manager.core.security.SecurityExpressions;
import ru.qwerty.internship.manager.core.service.DiaryService;
import ru.qwerty.internship.manager.core.util.ResourceUtils;
import ru.qwerty.internship.manager.data.model.diary.DiaryResponse;
import ru.qwerty.internship.manager.data.model.resource.ResourceResponse;

import java.util.List;

@RestController
@RequestMapping("/diaries")
@RequiredArgsConstructor
public class DiaryController {

    private final DiaryService service;

    @GetMapping("/all")
    @PreAuthorize(SecurityExpressions.ROLE_DEAN)
    public List<DiaryResponse> getAll() {
        return service.get();
    }

    @GetMapping
    @PreAuthorize(SecurityExpressions.ROLE_STUDENT)
    public List<DiaryResponse> get() {
        return service.get(ExecutionContextHolder.get().getUser().getId());
    }

    @GetMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_DEAN_AND_STUDENT)
    public ResponseEntity<byte[]> get(@PathVariable long id) {
        return service.get(ExecutionContextHolder.get().getUser().getId(), id);
    }

    @PostMapping
    @PreAuthorize(SecurityExpressions.ROLE_STUDENT)
    public void create(@RequestParam(name = "file") MultipartFile file) {
        service.create(file);
    }
}
