package ru.qwerty.internship.manager.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.qwerty.internship.manager.config.property.TokenProperties;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;
import ru.qwerty.internship.manager.data.model.auth.TokenType;
import ru.qwerty.internship.manager.exception.UnauthorizedException;
import ru.qwerty.internship.manager.security.JwtService;

import java.security.Key;
import java.util.Date;
import java.util.Map;

@Service
public class CommonJwtService implements JwtService {
    
    private final TokenProperties authTokenProperties;

    private final TokenProperties refreshTokenProperties;

    public CommonJwtService(
            @Qualifier("authTokenProperties") TokenProperties authTokenProperties,
            @Qualifier("refreshTokenProperties") TokenProperties refreshTokenProperties
    ) {
        this.authTokenProperties = authTokenProperties;
        this.refreshTokenProperties = refreshTokenProperties;
    }

    @Override
    public void validateAuthToken(String token) {
        validateToken(token, authTokenProperties);
    }

    @Override
    public void validateRefreshToken(String token) {
        validateToken(token, refreshTokenProperties);
    }

    private void validateToken(String token, TokenProperties tokenProperties) {
        if (extractClaims(token, tokenProperties).getExpiration().before(new Date())) {
            throw new UnauthorizedException();
        }
    }

    @Override
    public TokenPair generateTokenPair(UserEntity user) {
        return new TokenPair(
                buildToken(user.getUsername(), Map.of(JwtClaimsNames.ROLE, user.getRole()), authTokenProperties),
                buildToken(user.getUsername(), Map.of(), refreshTokenProperties)
        );
    }

    private String buildToken(String username, Map<String, Object> claims, TokenProperties tokenProperties) {
        return Jwts.builder()
                .setSubject(username)
                .addClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + tokenProperties.getExpirationTime()))
                .signWith(getSignInKey(tokenProperties.getSecretKey()), SignatureAlgorithm.HS256)
                .compact();
    }

    @Override
    public Claims extractClaims(String token, TokenType tokenType) {
        return extractClaims(token, getTokenProperties(tokenType));
    }

    private Claims extractClaims(String token, TokenProperties tokenProperties) {
        return Jwts.parserBuilder()
                .setSigningKey(getSignInKey(tokenProperties.getSecretKey()))
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignInKey(String secretKey) {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    private TokenProperties getTokenProperties(TokenType tokenType) {
        return tokenType == TokenType.AUTHORIZATION ? authTokenProperties : refreshTokenProperties;
    }
}
