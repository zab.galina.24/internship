package ru.qwerty.internship.manager.exception;

import org.springframework.http.HttpStatus;

public class DataConflictException extends ServiceException {

    public DataConflictException(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
