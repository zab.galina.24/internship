package ru.qwerty.internship.manager.core.security;

public final class SecurityExpressions {

    public static final String ROLE_STUDENT = "hasRole('STUDENT')";

    public static final String ROLE_DEAN = "hasAnyRole('DEAN', 'ADMIN')";

    public static final String ROLE_ALL = "hasAnyRole('DEAN', 'ADMIN', 'STUDENT', 'COMPANY')";

    public static final String ROLE_DEAN_AND_STUDENT = "hasAnyRole('DEAN', 'ADMIN', 'STUDENT')";

    public static final String ROLE_COMPANY = "hasAnyRole('COMPANY', 'ADMIN')";

    public static final String ROLE_ADMIN = "hasRole('ADMIN')";

    private SecurityExpressions() {
    }
}
