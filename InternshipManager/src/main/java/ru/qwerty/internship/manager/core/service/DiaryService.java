package ru.qwerty.internship.manager.core.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import ru.qwerty.internship.manager.data.model.diary.DiaryResponse;
import ru.qwerty.internship.manager.data.model.resource.ResourceResponse;

import java.util.List;

public interface DiaryService {

    DiaryResponse create(MultipartFile file);

    ResponseEntity<byte[]> get(long studentId, long diaryId);

    List<DiaryResponse> get(Long studentId);

    List<DiaryResponse> get();

}
