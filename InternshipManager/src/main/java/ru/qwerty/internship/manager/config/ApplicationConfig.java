package ru.qwerty.internship.manager.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationEntryPointFailureHandler;
import org.springframework.security.web.authentication.AuthenticationFilter;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import ru.qwerty.internship.manager.config.property.AdminProperties;
import ru.qwerty.internship.manager.config.property.TokenProperties;
import ru.qwerty.internship.manager.security.jwt.JwtAuthenticationConverter;
import ru.qwerty.internship.manager.security.jwt.JwtAuthenticationManager;
import ru.qwerty.internship.manager.security.jwt.JwtAuthenticationSuccessHandler;

@Configuration
public class ApplicationConfig {

    @Bean
    public AuthenticationFilter authenticationFilter(
            JwtAuthenticationManager authenticationManager,
            JwtAuthenticationConverter authenticationConverter,
            JwtAuthenticationSuccessHandler authenticationSuccessHandler
    ) {
        AuthenticationFilter filter = new AuthenticationFilter(authenticationManager, authenticationConverter);
        filter.setSuccessHandler(authenticationSuccessHandler);
        HttpStatusEntryPoint httpStatusEntryPoint = new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
        filter.setFailureHandler(new AuthenticationEntryPointFailureHandler(httpStatusEntryPoint));
        return filter;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @ConfigurationProperties(prefix = "service.security.jwt.auth-token")
    public TokenProperties authTokenProperties() {
        return new TokenProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "service.security.jwt.refresh-token")
    public TokenProperties refreshTokenProperties() {
        return new TokenProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "service.admin")
    public AdminProperties adminProperties() {
        return new AdminProperties();
    }
}
