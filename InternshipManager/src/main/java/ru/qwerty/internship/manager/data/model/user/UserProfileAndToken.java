package ru.qwerty.internship.manager.data.model.user;

import lombok.*;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Jacksonized
public class UserProfileAndToken {

    public UserProfileAndToken(){}

//    public UserProfileAndToken(){}

    private TokenPair tokenPair;

    private UserProfile userProfile;
}
