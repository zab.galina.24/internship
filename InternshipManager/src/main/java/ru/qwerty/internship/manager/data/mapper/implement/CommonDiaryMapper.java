package ru.qwerty.internship.manager.data.mapper.implement;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.qwerty.internship.manager.core.context.ExecutionContextHolder;
import ru.qwerty.internship.manager.data.entity.DiaryEntity;
import ru.qwerty.internship.manager.data.mapper.DiaryMapper;
import ru.qwerty.internship.manager.data.model.diary.DiaryResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Service
public class CommonDiaryMapper implements DiaryMapper {

    private static final String DATE_PATTERN = "dd.MM.yyyy";

    @Override
    public DiaryEntity map(MultipartFile file) {
        return DiaryEntity.builder()
                .originalName(file.getOriginalFilename())
                .path(UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename()))
                .uploadDate(new Date())
                .student(ExecutionContextHolder.get().getUser().cast())
                .build();
    }

    @Override
    public DiaryResponse map(DiaryEntity entity) {
        return DiaryResponse.builder()
                .id(entity.getId())
                .filename(entity.getOriginalName())
                .uploadDate(formatDate(entity.getUploadDate()))
                .studentId(entity.getStudent().getId())
                .build();
    }

    private static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        return format.format(date);
    }
}
