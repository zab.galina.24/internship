package ru.qwerty.internship.manager.data;

public final class Parameters {

    public static final String GROUP_PREFIX = "97";

    public static final int GROUP_LENGTH = 6;

    public static final long MIN_PREFERENCE_ORDER_NUMBER = 1;

    public static final long MAX_PREFERENCE_ORDER_NUMBER = 10;

    private Parameters() {
    }
}
