package ru.qwerty.internship.manager.api;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.qwerty.internship.manager.core.security.SecurityExpressions;
import ru.qwerty.internship.manager.core.service.CompanyService;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.company.CreateCompanyRequest;
import ru.qwerty.internship.manager.data.model.company.EditCompanyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.CreateVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.EditVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

@RestController
@RequestMapping("/companies")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService service;

    private final VacancyService vacancyService;

    @GetMapping
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public List<CompanyResponse> get() {
        return service.get();
    }

    @GetMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public CompanyResponse getById(@PathVariable long id) {
        return service.getById(id);
    }

    @PostMapping
    @PreAuthorize(SecurityExpressions.ROLE_ADMIN)
    public CompanyResponse create(@Valid @RequestBody CreateCompanyRequest request) {
        return service.create(request);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ADMIN)
    public void delete(@PathVariable long id) {
        service.delete(id);
    }

    @PatchMapping("/{id}")
    public CompanyResponse edit(@Valid @RequestBody EditCompanyRequest request, @PathVariable long id) {
        return service.patch(id, request);
    }

    @GetMapping("/{companyId}/vacancies")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public List<VacancyResponse> getVacancies(@PathVariable long companyId) {
        return service.getVacancies(companyId);
    }

    @GetMapping("/{companyId}/vacancies/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public VacancyResponse getVacancy(@PathVariable long companyId, @PathVariable long id) {
        return vacancyService.getById(id);
    }

    @PostMapping("/{companyId}/vacancies")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public VacancyResponse createVacancy(
            @Valid @RequestBody CreateVacancyRequest request,
            @PathVariable long companyId
    ) {
        return vacancyService.create(request, companyId);
    }

    @PatchMapping("/{companyId}/vacancies/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public VacancyResponse editVacancy(
            @PathVariable long id,
            @PathVariable long companyId,
            @Valid @RequestBody EditVacancyRequest request) {
        return vacancyService.edit(id, request);
    }

    @DeleteMapping("/{companyId}/vacancies/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public void deleteVacancy(@PathVariable long id, @PathVariable long companyId) {
        vacancyService.delete(id);
    }
}
