package ru.qwerty.internship.manager.data.model.user;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.entity.Role;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;

import java.util.List;

@Data
@Builder
@Jacksonized
public class UserProfileWithPreferences  {

    private Long id;

    private byte[] profilePhoto;

    private String username;

    private String firstname;

    private String patronymic;

    private String lastname;

    private Role role;

    private String birthDate;

    private double performance;

    private int studyYear;

    private String groupNumber;

    private String internshipCompany;

    private String responsibleForCompany;

    private String placeOfWork;

    private String jobTitle;

    private List<PreferenceResponse> preferences;

}
