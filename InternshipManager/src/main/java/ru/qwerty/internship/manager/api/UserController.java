package ru.qwerty.internship.manager.api;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.qwerty.internship.manager.core.security.SecurityExpressions;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.data.mapper.UserMapper;
import ru.qwerty.internship.manager.data.model.user.CreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.ExtendedCreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserResponse;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    private final UserMapper mapper;

    @PostMapping("/role/admin")
    @PreAuthorize(SecurityExpressions.ROLE_ADMIN)
    public UserResponse create(@Valid @RequestBody ExtendedCreateUserRequest request) {
        return mapper.map(service.create(mapper.map(request)));
    }

    @GetMapping("/students")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public List<UserProfile> getStudents() {
        return service.getStudents();
    }

    @GetMapping("/{id}")
    @PreAuthorize(SecurityExpressions.ROLE_ALL)
    public UserProfile getStudents(@PathVariable long id) {
        return service.getProfileById(id);
    }
}
