package ru.qwerty.internship.manager.core.service.implement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.qwerty.internship.manager.core.context.ExecutionContextHolder;
import ru.qwerty.internship.manager.core.repository.PreferenceRepository;
import ru.qwerty.internship.manager.core.service.PreferenceService;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.entity.PreferenceEntity;
import ru.qwerty.internship.manager.data.entity.StudentEntity;
import ru.qwerty.internship.manager.data.mapper.PreferenceMapper;
import ru.qwerty.internship.manager.data.model.preference.CreatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;
import ru.qwerty.internship.manager.data.model.preference.UpdatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserProfileWithPreferences;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonPreferenceService implements PreferenceService {

    private static final String PREFERENCE_NOT_FOUND_MESSAGE_TEMPLATE = "Preference with id = %d not found";

    private final PreferenceRepository preferenceRepository;

    private final PreferenceMapper mapper;

    private final VacancyService vacancyService;

    private final UserService userService;

    @Override
    @Transactional(readOnly = true)
    public List<PreferenceResponse> get() {
        StudentEntity student = ExecutionContextHolder.get().getUser().cast();
        log.info("Get preferences by student = {}", student.getUsername());

        return preferenceRepository.getAllByStudent(student)
                .stream()
                .map(mapper::map)
                .toList();
    }

    @Override
    public List<UserProfileWithPreferences> getAll() {
        List<UserProfile> userProfiles = userService.getStudents();
        List<UserProfileWithPreferences> result = new ArrayList<>(List.of());
        for (UserProfile userProfile: userProfiles) {
            StudentEntity studentEntity = userService.getById(userProfile.getId()).cast();
            List<PreferenceResponse> preferenceResponses = preferenceRepository.getAllByStudent(studentEntity)
                    .stream()
                    .map(mapper::map)
                    .toList();
            UserProfileWithPreferences userProfileWithPreferences = UserProfileWithPreferences.builder()
                    .id(userProfile.getId())
                    .profilePhoto(userProfile.getProfilePhoto())
                    .username(userProfile.getUsername())
                    .lastname(userProfile.getLastname())
                    .firstname(userProfile.getFirstname())
                    .patronymic(userProfile.getPatronymic())
                    .birthDate(userProfile.getBirthDate())
                    .performance(userProfile.getPerformance())
                    .studyYear(userProfile.getStudyYear())
                    .groupNumber(userProfile.getGroupNumber())
                    .internshipCompany(userProfile.getInternshipCompany())
                    .responsibleForCompany(userProfile.getResponsibleForCompany())
                    .placeOfWork(userProfile.getPlaceOfWork())
                    .jobTitle(userProfile.getJobTitle())
                    .role(userProfile.getRole())
                    .preferences(preferenceResponses)
                    .build();
            result.add(userProfileWithPreferences);
        }
        return result;
    }

    @Override
    @Transactional
    public PreferenceResponse create(CreatePreferenceRequest request) {
        log.info("Create preference: = {}", request);
        PreferenceEntity entity = preferenceRepository.save(mapper.map(request));
        return mapper.map(entity);
    }

    @Override
    @Transactional
    public PreferenceResponse update(UpdatePreferenceRequest request) {
        log.info("Update preferences = {}", request);
        StudentEntity student = ExecutionContextHolder.get().getUser().cast();
        PreferenceEntity preference = preferenceRepository.getByIdAndStudent(request.getId(), student).orElseThrow(
                () -> new ObjectNotFoundException(PREFERENCE_NOT_FOUND_MESSAGE_TEMPLATE)
        );

        if (request.getVacancyId() != null) {
            preference.setVacancy(vacancyService.getEntityById(request.getId()));
        }
        if (request.getOrderNumber() != null) {
            preference.setOrderNumber(request.getOrderNumber());
        }
        if (request.getStatus() != null) {
            preference.setStatus(request.getStatus());
        }

        return mapper.map(preference);
    }
}
