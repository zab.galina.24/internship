package ru.qwerty.internship.manager.core.service;

import ru.qwerty.internship.manager.data.model.auth.LoginRequest;
import ru.qwerty.internship.manager.data.model.auth.RegisterRequest;
import ru.qwerty.internship.manager.data.model.auth.Token;
import ru.qwerty.internship.manager.data.model.auth.TokenPair;
import ru.qwerty.internship.manager.data.model.user.UserProfileAndToken;

public interface AuthService {

    TokenPair register(RegisterRequest request);

    UserProfileAndToken login(LoginRequest request);

    TokenPair refreshToken(Token token);
}
