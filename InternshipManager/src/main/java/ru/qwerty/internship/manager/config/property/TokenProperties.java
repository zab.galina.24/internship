package ru.qwerty.internship.manager.config.property;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenProperties {

    private String secretKey;

    private int expirationTime;

}
