package ru.qwerty.internship.manager.data.model.diary;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder(builderClassName = "Builder")
public class DiaryResponse {

    long id;

    String filename;

    String uploadDate;

    long studentId;
}
