package ru.qwerty.internship.manager.data.model.auth;

public record TokenPair(String authToken, String refreshToken) {
}
