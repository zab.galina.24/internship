package ru.qwerty.internship.manager.core.service.implement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.qwerty.internship.manager.core.repository.UserRepository;
import ru.qwerty.internship.manager.core.service.UserService;
import ru.qwerty.internship.manager.data.entity.Role;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.mapper.UserMapper;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.exception.DataConflictException;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonUserService implements UserService {

    private static final String USER_NOT_FOUND_BY_ID_MESSAGE_TEMPLATE = "User with id = %s not found";

    private static final String USER_NOT_FOUND_BY_USERNAME_MESSAGE_TEMPLATE = "User with username = %s not found";

    private static final String USER_ALREADY_EXISTS_MESSAGE_TEMPLATE = "User with username = %s already exists";

    private final UserRepository repository;

    private final UserMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public UserEntity getById(long id) {
        log.info("Get user by id = {}", id);
        return repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(String.format(USER_NOT_FOUND_BY_ID_MESSAGE_TEMPLATE, id))
        );
    }

    @Override
    public UserProfile getProfileById(long id) {
        log.info("Get user by id = {}", id);
        return mapper.mapToProfile(repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(String.format(USER_NOT_FOUND_BY_ID_MESSAGE_TEMPLATE, id))));
    }

    @Override
    @Transactional(readOnly = true)
    public UserEntity getByUsername(String username) {
        log.info("Get user by username = {}", username);
        return repository.findByUsername(username).orElseThrow(
                () -> new ObjectNotFoundException(String.format(USER_NOT_FOUND_BY_USERNAME_MESSAGE_TEMPLATE, username))
        );
    }

    @Override
    @Transactional
    public UserEntity create(UserEntity user) {
        log.info("Create user = {}", user);
        Optional<UserEntity> existingUser = repository.findByUsername(user.getUsername());
        if (existingUser.isPresent()) {
            throw new DataConflictException(String.format(USER_ALREADY_EXISTS_MESSAGE_TEMPLATE, user.getUsername()));
        }

        return repository.save(user);
    }

    @Override
    public List<UserProfile> getStudents() {
        return repository.findAll()
                .stream()
                .filter(userEntity -> userEntity.getRole() == Role.STUDENT)
                .map(mapper::mapToProfile)
                .toList();
    }

    @Override
    @Transactional
    public void deleteByUsername(String username) {
        log.info("Delete user by username = {}", username);
        UserEntity userEntity = getByUsername(username);
        repository.delete(userEntity);
    }
}
