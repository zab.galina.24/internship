package ru.qwerty.internship.manager.core.service;

import ru.qwerty.internship.manager.data.entity.VacancyEntity;
import ru.qwerty.internship.manager.data.model.vacancy.CreateVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.EditVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;

import java.util.List;

public interface VacancyService {

    List<VacancyResponse> get();

    VacancyResponse getById(long id);

    VacancyEntity getEntityById(long id);

    List<VacancyEntity> getEntitiesByCompanyId(long id);

    VacancyResponse create(CreateVacancyRequest vacancy, long companyId);

    VacancyResponse edit(long id, EditVacancyRequest request);

    void delete(long id);
}
