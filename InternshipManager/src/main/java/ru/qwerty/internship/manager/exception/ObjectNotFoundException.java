package ru.qwerty.internship.manager.exception;

import org.springframework.http.HttpStatus;

public class ObjectNotFoundException extends ServiceException {

    public ObjectNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
