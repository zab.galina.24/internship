package ru.qwerty.internship.manager.data.entity;

import jakarta.persistence.*;
import lombok.*;
import ru.qwerty.internship.manager.data.model.preference.Status;

@Getter
@Setter
@Entity
@Builder
@Table(name = "preferences")
@NoArgsConstructor
@AllArgsConstructor
public class PreferenceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "vacancy_id", referencedColumnName = "id")
    private VacancyEntity vacancy;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity student;

    private int orderNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

}
