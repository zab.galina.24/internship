package ru.qwerty.internship.manager.core.service;

import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.model.user.UserProfile;

import java.util.List;

public interface UserService {

    UserEntity getById(long id);

    UserProfile getProfileById(long id);

    UserEntity getByUsername(String username);

    UserEntity create(UserEntity user);

    List<UserProfile> getStudents();

    void deleteByUsername(String username);
}
