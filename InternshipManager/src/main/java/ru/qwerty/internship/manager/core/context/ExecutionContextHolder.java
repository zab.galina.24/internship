package ru.qwerty.internship.manager.core.context;

public class ExecutionContextHolder {

    private static final ThreadLocal<ExecutionContext> CONTEXT = ThreadLocal.withInitial(ExecutionContext::new);

    private ExecutionContextHolder() {
    }

    public static ExecutionContext get() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }
}
