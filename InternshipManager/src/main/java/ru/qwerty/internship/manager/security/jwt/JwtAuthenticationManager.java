package ru.qwerty.internship.manager.security.jwt;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.data.model.auth.TokenType;
import ru.qwerty.internship.manager.exception.UnauthorizedException;
import ru.qwerty.internship.manager.security.JwtService;

import java.util.List;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationManager implements AuthenticationManager {

    private static final String ROLE_PREFIX = "ROLE_";

    private final JwtService jwtService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication instanceof JwtAuthenticationToken jwtAuthenticationToken) {
            jwtService.validateAuthToken(jwtAuthenticationToken.getCredentials());
            Claims claims = jwtService.extractClaims(jwtAuthenticationToken.getCredentials(), TokenType.AUTHORIZATION);
            return new JwtAuthenticationToken(claims, createAuthorities(claims.get(JwtClaimsNames.ROLE, String.class)));
        }

        throw new UnauthorizedException();
    }

    private static List<GrantedAuthority> createAuthorities(String role) {
        return List.of(new SimpleGrantedAuthority(ROLE_PREFIX + role));
    }
}
