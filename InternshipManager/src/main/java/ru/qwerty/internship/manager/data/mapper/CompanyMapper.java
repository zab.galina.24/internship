package ru.qwerty.internship.manager.data.mapper;

import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.model.company.CompanyResponse;
import ru.qwerty.internship.manager.data.model.company.CreateCompanyRequest;

public interface CompanyMapper {

    CompanyEntity map(CreateCompanyRequest createCompanyRequest);

    CompanyResponse map(CompanyEntity companyEntity);
}
