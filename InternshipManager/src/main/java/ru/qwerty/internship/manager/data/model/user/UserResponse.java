package ru.qwerty.internship.manager.data.model.user;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import ru.qwerty.internship.manager.data.entity.Role;

@Value
@Builder
@Jacksonized
public class UserResponse {

    Long id;

    String username;

    String firstname;

    String patronymic;

    String lastname;

    Role role;

}
