package ru.qwerty.internship.manager.core.service.implement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.qwerty.internship.manager.core.repository.CompanyRepository;
import ru.qwerty.internship.manager.core.repository.VacancyRepository;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.entity.CompanyEntity;
import ru.qwerty.internship.manager.data.entity.VacancyEntity;
import ru.qwerty.internship.manager.data.mapper.VacancyMapper;
import ru.qwerty.internship.manager.data.model.vacancy.CreateVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.EditVacancyRequest;
import ru.qwerty.internship.manager.data.model.vacancy.VacancyResponse;
import ru.qwerty.internship.manager.exception.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommonVacancyService implements VacancyService {

    private static final String COMPANY_NOT_FOUND_MESSAGE_TEMPLATE = "Company with id = %d not found";

    private static final String VACANCY_NOT_FOUND_MESSAGE_TEMPLATE = "Vacancy with id = %d not found";

    private final VacancyRepository repository;

    private final CompanyRepository companyRepository;

    private final VacancyMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public List<VacancyResponse> get() {
        log.info("Get all vacancies");
        List<VacancyResponse> vacancies = new ArrayList<>();
        repository.findAll().forEach(vacancyEntity -> vacancies.add(mapper.map(vacancyEntity)));
        return vacancies;
    }

    @Override
    @Transactional(readOnly = true)
    public VacancyResponse getById(long id) {
        log.info("Get vacancy by id = {}", id);
        VacancyEntity vacancyEntity = getEntityById(id);
        return mapper.map(vacancyEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public VacancyEntity getEntityById(long id) {
        log.info("Get vacancy entity by id = {}", id);
        return repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(String.format(VACANCY_NOT_FOUND_MESSAGE_TEMPLATE, id))
        );
    }

    @Override
    public List<VacancyEntity> getEntitiesByCompanyId(long id) {
        CompanyEntity companyEntity = companyRepository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(String.format(COMPANY_NOT_FOUND_MESSAGE_TEMPLATE, id))
        );
        return companyEntity.getVacancies();
    }

    @Override
    @Transactional
    public VacancyResponse create(CreateVacancyRequest vacancy, long companyId) {
        log.info("Create vacancy = {}", vacancy);
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(
                () -> new ObjectNotFoundException(String.format(COMPANY_NOT_FOUND_MESSAGE_TEMPLATE, companyId))
        );
        VacancyEntity vacancyEntity = mapper.map(vacancy, companyEntity);
        return mapper.map(repository.save(vacancyEntity));
    }

    @Override
    @Transactional
    public VacancyResponse edit(long id, EditVacancyRequest request) {
        VacancyEntity vacancyEntity = getEntityById(id);
        if (request.getName().isPresent()) {
            vacancyEntity.setName(request.getName().get());
        }
        if (request.getTechStack().isPresent()) {
            vacancyEntity.setTechStack(request.getTechStack().get());
        }
        if (request.getMinimumQuantity() != null && request.getMinimumQuantity().isPresent()) {
            vacancyEntity.setMinimumQuality(request.getMinimumQuantity().get());
        }
        if (request.getMaximumQuantity() != null && request.getMaximumQuantity().isPresent()) {
            vacancyEntity.setMaximumQuality(request.getMaximumQuantity().get());
        }
        repository.save(vacancyEntity);

        return mapper.map(vacancyEntity);
    }

    @Override
    @Transactional
    public void delete(long id) {
        log.info("Delete vacancy by id = {}", id);
        VacancyEntity vacancyEntity = getEntityById(id);
        repository.delete(vacancyEntity);
    }
}
