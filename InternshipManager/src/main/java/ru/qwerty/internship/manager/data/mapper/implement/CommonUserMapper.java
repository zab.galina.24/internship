package ru.qwerty.internship.manager.data.mapper.implement;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.core.service.CompanyService;
import ru.qwerty.internship.manager.data.Messages;
import ru.qwerty.internship.manager.data.entity.CompanyDelegateEntity;
import ru.qwerty.internship.manager.data.entity.Role;
import ru.qwerty.internship.manager.data.entity.StudentEntity;
import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.mapper.UserMapper;
import ru.qwerty.internship.manager.data.model.auth.RegisterRequest;
import ru.qwerty.internship.manager.data.model.user.CreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.ExtendedCreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserResponse;
import ru.qwerty.internship.manager.exception.InvalidArgumentException;

@Component
@RequiredArgsConstructor
public class CommonUserMapper implements UserMapper {

    private final CompanyService companyService;

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserEntity map(RegisterRequest request) {
        return StudentEntity.builder()
                .username(request.getUsername())
                .hashedPassword(passwordEncoder.encode(request.getPassword()))
                .firstname(request.getFirstname())
                .patronymic(request.getPatronymic())
                .lastname(request.getLastname())
                .groupNumber(request.getGroupNumber())
                .role(Role.UNVERIFIED_STUDENT)
                .build();
    }

    @Override
    public UserEntity map(CreateUserRequest request) {
        return createUser(StudentEntity.builder().role(Role.STUDENT), request);
    }

    @Override
    public UserEntity map(ExtendedCreateUserRequest request) {
        return createUser(createUserEntityBuilder(request), request);
    }

    private UserEntity createUser(UserEntity.UserEntityBuilder<?, ?> builder, CreateUserRequest request) {
        return builder.username(request.getUsername())
                .hashedPassword(passwordEncoder.encode(request.getPassword()))
                .firstname(request.getFirstname())
                .patronymic(request.getPatronymic())
                .lastname(request.getLastname())
                .profilePhoto(request.getProfilePhoto())
                .responsibleForCompany(request.getResponsibleForCompany())
                .build();
    }

    @Override
    public UserResponse map(UserEntity userEntity) {
        return UserResponse.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .firstname(userEntity.getFirstname())
                .patronymic(userEntity.getPatronymic())
                .lastname(userEntity.getLastname())
                .role(userEntity.getRole())
                .build();
    }

    @Override
    public UserProfile mapToProfile(UserEntity userEntity) {
        return UserProfile.builder()
                .id(userEntity.getId())
                .profilePhoto(userEntity.getProfilePhoto())
                .username(userEntity.getUsername())
                .lastname(userEntity.getLastname())
                .firstname(userEntity.getFirstname())
                .patronymic(userEntity.getPatronymic())
                .birthDate(userEntity.getBirthDate())
                .performance(userEntity.getPerformance())
                .studyYear(userEntity.getStudyYear())
                .groupNumber(userEntity.getGroupNumber())
                .internshipCompany(userEntity.getInternshipCompany())
                .responsibleForCompany(userEntity.getResponsibleForCompany())
                .placeOfWork(userEntity.getPlaceOfWork())
                .jobTitle(userEntity.getJobTitle())
                .role(userEntity.getRole())
                .build();
    }

    private UserEntity.UserEntityBuilder<?, ?> createUserEntityBuilder(ExtendedCreateUserRequest request) {
        if (request.getRole() == Role.COMPANY) {
            if (request.getCompanyId() == null) {
                throw new InvalidArgumentException(Messages.INVALID_COMPANY_ID);
            }
            return CompanyDelegateEntity.builder()
                    .role(Role.COMPANY)
                    .placeOfWork(companyService.getEntityById(request.getCompanyId()).getName())
                    .jobTitle(request.getJobTitle())
                    .company(companyService.getEntityById(request.getCompanyId()));
        }

        if (request.getRole() == Role.STUDENT || request.getRole() == Role.UNVERIFIED_STUDENT) {
            if (request.getGroupNumber() == null) {
                throw new InvalidArgumentException(Messages.INVALID_GROUP_NUMBER);
            }
            return StudentEntity.builder()
                    .birthDate(request.getBirthDate())
                    .performance(request.getPerformance())
                    .studyYear(request.getStudyYear())
                    .internshipCompany(request.getInternshipCompany())
                    .role(request.getRole())
                    .groupNumber(request.getGroupNumber());
        }

        return UserEntity.builder().role(request.getRole());
    }
}
