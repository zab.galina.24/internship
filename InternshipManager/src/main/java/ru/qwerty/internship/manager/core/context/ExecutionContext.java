package ru.qwerty.internship.manager.core.context;

import lombok.Getter;
import lombok.Setter;
import ru.qwerty.internship.manager.data.entity.UserEntity;

@Getter
@Setter
public class ExecutionContext {

    UserEntity user;
}
