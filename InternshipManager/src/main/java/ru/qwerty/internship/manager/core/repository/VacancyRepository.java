package ru.qwerty.internship.manager.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.qwerty.internship.manager.data.entity.VacancyEntity;

@Repository
public interface VacancyRepository extends CrudRepository<VacancyEntity, Long> {
}
