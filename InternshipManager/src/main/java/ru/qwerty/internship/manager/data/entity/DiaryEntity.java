package ru.qwerty.internship.manager.data.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Entity
@Builder
@Table(name = "diary")
@NoArgsConstructor
@AllArgsConstructor
public class DiaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String originalName;

    @Column(unique = true, nullable = false)
    private String path;

    @Temporal(TemporalType.DATE)
    private Date uploadDate;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    private StudentEntity student;
}
