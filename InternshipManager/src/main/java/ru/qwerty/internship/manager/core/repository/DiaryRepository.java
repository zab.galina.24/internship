package ru.qwerty.internship.manager.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.qwerty.internship.manager.data.entity.DiaryEntity;

@Repository
public interface DiaryRepository extends CrudRepository<DiaryEntity, Long> {
}
