package ru.qwerty.internship.manager.data.mapper;

import ru.qwerty.internship.manager.data.entity.UserEntity;
import ru.qwerty.internship.manager.data.model.auth.RegisterRequest;
import ru.qwerty.internship.manager.data.model.user.CreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.ExtendedCreateUserRequest;
import ru.qwerty.internship.manager.data.model.user.UserProfile;
import ru.qwerty.internship.manager.data.model.user.UserResponse;

public interface UserMapper {

    UserEntity map(RegisterRequest request);

    UserEntity map(CreateUserRequest request);

    UserEntity map(ExtendedCreateUserRequest request);

    UserResponse map(UserEntity userEntity);

    UserProfile mapToProfile (UserEntity userEntity);
}
