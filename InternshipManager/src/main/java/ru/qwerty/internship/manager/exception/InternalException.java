package ru.qwerty.internship.manager.exception;

import org.springframework.http.HttpStatus;

public class InternalException extends ServiceException {

    public InternalException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
