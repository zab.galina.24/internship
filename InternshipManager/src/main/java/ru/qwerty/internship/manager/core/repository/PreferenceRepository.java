package ru.qwerty.internship.manager.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.qwerty.internship.manager.data.entity.PreferenceEntity;
import ru.qwerty.internship.manager.data.entity.StudentEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface PreferenceRepository extends CrudRepository<PreferenceEntity, Long> {

    List<PreferenceEntity> getAllByStudent(StudentEntity entity);

    Optional<PreferenceEntity> getByIdAndStudent(long id, StudentEntity student);
}
