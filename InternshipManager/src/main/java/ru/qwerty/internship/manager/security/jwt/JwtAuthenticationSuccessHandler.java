package ru.qwerty.internship.manager.security.jwt;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.core.context.ExecutionContextHolder;
import ru.qwerty.internship.manager.core.service.UserService;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final UserService userService;

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication
    ) {
        log.info("Successful authentication");
        if (authentication instanceof JwtAuthenticationToken jwtAuthenticationToken) {
            String username = jwtAuthenticationToken.getPrincipal().getSubject();
            ExecutionContextHolder.get().setUser(userService.getByUsername(username));
        }
    }
}
