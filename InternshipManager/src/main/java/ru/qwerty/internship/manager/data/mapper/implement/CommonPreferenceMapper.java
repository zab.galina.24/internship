package ru.qwerty.internship.manager.data.mapper.implement;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.qwerty.internship.manager.core.context.ExecutionContextHolder;
import ru.qwerty.internship.manager.core.service.VacancyService;
import ru.qwerty.internship.manager.data.entity.PreferenceEntity;
import ru.qwerty.internship.manager.data.mapper.CompanyMapper;
import ru.qwerty.internship.manager.data.mapper.PreferenceMapper;
import ru.qwerty.internship.manager.data.mapper.VacancyMapper;
import ru.qwerty.internship.manager.data.model.preference.CreatePreferenceRequest;
import ru.qwerty.internship.manager.data.model.preference.PreferenceResponse;

@Component
@RequiredArgsConstructor
public class CommonPreferenceMapper implements PreferenceMapper {

    private final VacancyService vacancyService;

    private final VacancyMapper vacancyMapper;

    private final CompanyMapper companyMapper;

    @Override
    public PreferenceEntity map(CreatePreferenceRequest request) {
        return PreferenceEntity.builder()
                .vacancy(vacancyService.getEntityById(request.getVacancyId()))
                .student(ExecutionContextHolder.get().getUser().cast())
                .orderNumber(request.getOrderNumber())
                .status(request.getStatus())
                .build();
    }

    @Override
    public PreferenceResponse map(PreferenceEntity entity) {
        return PreferenceResponse.builder()
                .id(entity.getId())
                .vacancyId(entity.getVacancy().getId())
                .vacancy(vacancyMapper.map(entity.getVacancy()))
                .company(companyMapper.map(entity.getVacancy().getCompany()))
                .status(entity.getStatus())
                .studentId(entity.getStudent().getId())
                .orderNumber(entity.getOrderNumber())
                .build();
    }
}
